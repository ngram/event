package sf.event;
using stx.Log;

class Timer extends DefaultEventSystem{
	var timer 		: haxe.Timer;
	var interval 	: Int;
	var repeats 	: Int;
	var _running  : Bool;
	var count 		: Int;

	public function new(interval,repeats = 0){
		trace('new timer'.debug());
		this.interval = interval;
		this.repeats 	= repeats;
		this.count 		= 0;
		this._running = false;
		timer 				= new haxe.Timer(interval);
		timer.run 		= this.run;
		super();
	}
	private function run(){
		if (!_running) return;
		//trace('run : ' + repeats);
		if( (repeats!=0) ){
		 	if (count < repeats ){
				this.dispatchEvent( new Event('timer') );
				count++;
			}else{
				stop();
			}
		}else{
			this.dispatchEvent( new Event('timer') );
		}
	}
	public function start(){
		count   			= 	0;
		timer.stop();
		timer 				= new haxe.Timer(interval);
		_running 			= true;
		timer.run 		= this.run;
	}
	public function stop(){
		_running  = false;
		timer.stop();
	}
	public function running():Bool{
		return _running;
	}
}