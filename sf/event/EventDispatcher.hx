package sf.event;

interface EventDispatcher<T:Event>{
	public function dispatchEvent(m:T):Void;
}
