package sf.event;

@DefaultImplementation('sf.event.DefaultEventSystem')
interface EventSystem<T:Event> implements EventDispatcher<T>, implements EventListener<T>{

}