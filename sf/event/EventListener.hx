package sf.event;

interface EventListener<T:Event>{
	public function addEventListener(name:String,listener:T->Void,?params:Dynamic):Void;
	public function removeEventListener(name:String,listener:T->Void,?params:Dynamic):Void;
	public function hasEventListener(name:String):Bool;
}
