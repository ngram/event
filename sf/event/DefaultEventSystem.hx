package sf.event;

class DefaultEventSystem<T:Event> implements EventSystem<T>{
	
	private var listeners:Hash<List<T->Void>>;
	private var source:Dynamic;

	public function new (?source:Dynamic){
		this.source = source;
		this.listeners = new Hash();
	}
	public function addEventListener(name:String,method:T->Void,?params:Dynamic){
		//trace('addEventListener');
		if (!listeners.exists(name)){
			listeners.set(name,new List<T->Void>());
		}
		var list = listeners.get(name);
		var self = this;
		if (!Lambda.exists(list,function (x:T->Void):Bool{return Reflect.compareMethods(x,method);})){
			list.push(method);
		}
	}
	public function removeEventListener(name:String,method:T->Void,?params:Dynamic){
		//trace("removeEventListener");
		if (!listeners.exists(name)){
			return;
		}
		var v = listeners.get(name).filter(function(x){return Reflect.compareMethods(method,x);});
		listeners.set(name,v);
	}
	public function hasEventListener(name:String){
		return listeners.exists(name);
	}
	public function dispatchEvent(e:T){
		//trace("dispatchEvent");
		var these = listeners.get(e.name);
		e.source = source ;
		if (these!=null){
			for (listener in these){
				//trace(listener);
				Reflect.callMethod(null,listener,[e]);
			}
		}
	}	
}
